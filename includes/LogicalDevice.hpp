#pragma once

#include <vulkan/vulkan.h>
#include <map>

//#include 

class LogicalDevice {

public:

	enum class QueueType {
		Graphics,
		Present
	};

	static VkDevice Get();
	static VkQueue GetQueue(QueueType type);

	static void Init();
	static void Deinit();

private:

	static VkDevice _vkDevice;

	static std::map<QueueType, VkQueue> _queues;


#ifdef NDEBUG
	static constexpr bool _enableValidationLayers = false;
#else
	static constexpr bool _enableValidationLayers = true;
#endif

};