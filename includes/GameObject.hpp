#pragma once

#include <vector>
#include <memory>
#include <string>

#include "Transform.hpp"
#include "UpdateScript.hpp"


class GameObject {

public:
	GameObject();
	virtual void Update();
	Transform& transform();

	void AddUpdateScript(std::shared_ptr<UpdateScript> updateScript);

	const std::string& name() const;
	void setName(const std::string& name);

protected:

	Transform _transform;
	std::vector<std::shared_ptr<UpdateScript>> _updateScripts;

private:
	std::string _name;
	static int _NbGameObjects;
};