#pragma once

#include <map>
#include <GLFW/glfw3.h>

class Input {

public:

	enum class State {
		PRESSED,
		RELEASED,
		UNDEFINED
	};

	enum class Key {
		A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
		K0, K1, K2, K3, K4, K5, K6, K7, K8, K9,
		LeftCtrl, RightCtrl, LeftMaj, RightMaj, LeftAlt, RightAlt
	};

	static GLFWwindow* window;

	static State GetKey(Key key);

	
private:
	static std::map<Key, int> _keys;

};