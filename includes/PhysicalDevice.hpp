#pragma once

#include <vulkan/vulkan.h>

#include <memory>

class PhysicalDevice {

public:

	static const VkPhysicalDevice& Get();

	static void Init();
	static void Deinit(); 

private:

	static VkPhysicalDevice _vkPhysicalDevice;

	static bool _IsDeviceSuitable(const VkPhysicalDevice& device);
	static bool _CheckDeviceExtensionsSupport(VkPhysicalDevice device);
	static VkSampleCountFlagBits _GetMaxUsageSampleCount();

};
