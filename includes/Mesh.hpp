#pragma once

#include "GameObject.hpp"

#include <string>

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

#include <vector>
#include <unordered_map>

struct Vertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec3 color;
	glm::vec2 texCoord;

	static VkVertexInputBindingDescription getBindingDescription();
	static std::array<VkVertexInputAttributeDescription, 4> getAttributeDescriptions();
	bool operator==(const Vertex& other) const;
};


namespace std {
	template<> struct hash<Vertex> {
		size_t operator() (Vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
				(hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}

class Mesh : public GameObject {

public:

	void loadModel(const std::string& modelPath);


	void bind(VkCommandBuffer& commandBuffer);

	void Update();

	void cleanup(VkDevice _device);


private:

	void _createVertexBuffer();
	void _createIndexBuffer();
	void _createNormalBuffer();

	std::vector<Vertex> _vertices;
	std::vector<uint32_t> _indices;
	std::unordered_map<Vertex, uint32_t> _uniqueVertices;

	VkBuffer _vertexBuffer;
	VkDeviceMemory _vertexBufferMemory;
	VkBuffer _indexBuffer;
	VkDeviceMemory _indexBufferMemory;
	VkBuffer _normalBuffer;
	VkDeviceMemory _normalBufferMemory;
};