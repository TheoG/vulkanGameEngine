#pragma once

#include <glm/glm.hpp>

class Transform {

public:

	Transform();

	void setPosition(glm::vec3 position);
	void setRotation(glm::vec3 rotation);
	void setScale(glm::vec3 scale);

	void translate(glm::vec3 translation);

	const glm::mat4& getViewMatrix() const;

	const glm::vec3& position() const;
	const glm::vec3& rotation() const;
	const glm::vec3& scale() const;

private:
	
	glm::mat4 _modelMatrix;

	glm::vec3 _position;
	glm::vec3 _rotation;
	glm::vec3 _scale;

	void _updateModelMatrix();

};