#pragma once

#include "GameObject.hpp"

class Light : public GameObject {

public:
	void setColor(glm::vec3 color);
	glm::vec3 getColor() const;

protected:
	Light();

private:
	glm::vec3 _color;
	
};