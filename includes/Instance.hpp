#pragma once

#include <vulkan/vulkan.h>

#include <memory>
#include <vector>

class Instance {

public:

	static const VkInstance& Get();

	static void Init();
	static void Deinit();

private:

	static VkInstance _vkInstance;
	static VkDebugUtilsMessengerEXT _vkMessenger;


#ifdef NDEBUG
	static constexpr bool _enableValidationLayers = false;
#else
	static constexpr bool _enableValidationLayers = true;
#endif

	static void _SetupDebugMessenger();

	static std::vector<const char*> _GetRequiredExtensions();
	static bool _CheckValidationLayerSupport();

		
};