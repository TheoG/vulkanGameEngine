#pragma once

#include <vulkan/vulkan.h>

#include <optional>
#include <vector>

class Common {

public:
	
	struct QueueFamilyIndices {
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		bool isComplete() {
			return graphicsFamily.has_value() && presentFamily.has_value();
		}
	};

	struct SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

	static VkSurfaceKHR Surface;

	static VkCommandPool CommandPool;
	
	static VkSampleCountFlagBits MsaaSamples;

	static SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice device);
	
	static void CreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);
	static void CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	static uint32_t FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
	static QueueFamilyIndices Common::FindQueueFamilies(VkPhysicalDevice device);

	static VkCommandBuffer BeginSingleTimeCommands();
	static void EndSingleTimeCommands(VkCommandBuffer commandBuffer);


	static const std::vector<const char*> _DeviceExtensions;
	static const std::vector<const char*> _ValidationLayers;
private:

};
