#pragma once

#include "GameObject.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera: GameObject {

public:

	enum class Type {
		FREE,
		CUSTOM
	};

	Camera(int width, int height);

	const glm::mat4& view() const;
	const glm::mat4& projection() const;

	float near() const;
	float far() const;

	void setNear(float near);
	void setFar(float far);

	void resize(int width, int height);
	void freeCameraRotation(float xDelta, float yDelta);

	Type getType() const;
	void setType(Type type);

	void Update();
private:

	void _updateProjectionMatrix();
	void _updateViewMatrix();

	int _width;
	int _height;

	float _near;
	float _far;

	float _speed;
	float _sensitivity;
	glm::vec2 _lastCursorPos;
	bool _firstMouse;

	glm::vec3 _front;
	glm::vec3 _up;

	glm::mat4 _view;
	glm::mat4 _proj;

	Type _type;
};