#pragma	once

class Time {

public:
	static float DeltaTime;
	static float TotalTime;

	static void Update();

private:
	static float _LastFrame;
};