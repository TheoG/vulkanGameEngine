#pragma once

class GameObject;

class UpdateScript {

public:

	virtual void Update(GameObject*) = 0;

};