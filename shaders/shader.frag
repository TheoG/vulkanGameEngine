#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexColor;
layout(location = 2) in vec3 fragNormal;
layout(location = 3) in vec3 fragPosition;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler;

void main() {
	
	vec3 lightPosition = vec3(-100.0, 100.0, 0.0);


	float ambientStrength = 0.05;
	vec3 lightColor = vec3(1.0, 0.0, 0.0);

	vec3 objColor = vec3(1.0, 1.0, 1.0);
	// vec3 objColor = normalize(fragNormal);

	vec3 ambient = ambientStrength * lightColor;


	vec3 norm = normalize(fragNormal);
	vec3 lightDir = normalize(lightPosition - fragPosition);
	
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;


	// vec3 result = (ambient + diffuse) * lightColor;
	vec3 result = (ambient + diffuse) * texture(texSampler, fragTexColor).xyz;
	outColor = vec4(result, 1.0);

	// outColor = vec4(fragTexColor, 0.0, 1.0);
	// outColor = vec4(fragColor, 1.0);
	// outColor = vec4(1.0, 0.0, 0.0, 1.0);
	// outColor = texture(texSampler, fragTexColor);
}