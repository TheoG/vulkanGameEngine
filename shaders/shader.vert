#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inNormal;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexColor;
layout(location = 2) out vec3 fragNormal;
layout(location = 3) out vec3 fragPosition;

layout(push_constant) uniform ModelMatrix {
	mat4 model;
} PushConstant;

layout(binding = 0) uniform UniformBufferObject {
	mat4 view;
	mat4 proj;
} ubo;

void main() {
	gl_Position = ubo.proj * ubo.view * PushConstant.model * vec4(inPosition, 1.0);
	fragColor = inColor;
	fragTexColor = inTexCoord;
	fragNormal = mat3(transpose(inverse(PushConstant.model))) * inNormal;
	fragPosition = vec3(PushConstant.model * vec4(inPosition, 1.0));
}