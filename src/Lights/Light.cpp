#include "Lights/Light.hpp"

Light::Light() {}

glm::vec3 Light::getColor() const {
	return _color;
}

void Light::setColor(glm::vec3 color) {
	_color = glm::normalize(color);
}
