#include "Transform.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

Transform::Transform() {
	_position = { 0.0f, 0.0f, 0.0f };
	_rotation = { 0.0f, 0.0f, 0.0f };
	_scale = { 1.0f, 1.0f, 1.0f };
	_updateModelMatrix();
}

void Transform::setPosition(glm::vec3 position) {
	_position = position;
	_updateModelMatrix();
}

void Transform::setRotation(glm::vec3 rotation) {
	_rotation = rotation;
	_updateModelMatrix();
}

void Transform::setScale(glm::vec3 scale) {
	_scale = scale;
	_updateModelMatrix();
}

void Transform::translate(glm::vec3 translation) {
	_position += translation;
	_updateModelMatrix();
}

const glm::mat4& Transform::getViewMatrix() const {
	return _modelMatrix;
}

const glm::vec3& Transform::position() const {
	return _position;
}

const glm::vec3& Transform::rotation() const {
	return _rotation;
}

const glm::vec3& Transform::scale() const {
	return _scale;
}


void Transform::_updateModelMatrix() {
	
	glm::mat4 translateMatrix = glm::translate(glm::mat4(1.0f), _position);
	glm::mat4 scaleMatrix = glm::scale(_scale);
	//glm::mat4 rotationMatrix = glm::rotate(glm::mat4(), _rotation); // TODO !!!!
	glm::mat4 rotationMatrix = glm::mat4(1.0f);

	_modelMatrix = scaleMatrix * rotationMatrix * translateMatrix;
}