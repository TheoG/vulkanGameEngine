#include "Time.hpp"
#include <GLFW/glfw3.h>

void Time::Update() {
	float currentFrame = static_cast<float>(glfwGetTime());
	Time::DeltaTime = currentFrame - Time::_LastFrame;
	Time::_LastFrame = currentFrame;
}

float Time::DeltaTime = 0.0f;
float Time::TotalTime = 0.0f;
float Time::_LastFrame = 0.0f;