#include "LogicalDevice.hpp"

#include "PhysicalDevice.hpp"
#include "Common.hpp"

#include <string>
#include <vector>
#include <set>
#include <stdexcept>


VkDevice LogicalDevice::Get() {
	return LogicalDevice::_vkDevice;
}

VkQueue LogicalDevice::GetQueue(LogicalDevice::QueueType type) {
	auto queue = LogicalDevice::_queues.find(type);
	if (queue != LogicalDevice::_queues.end()) {
		return queue->second;
	}
	return VK_NULL_HANDLE;
}

void LogicalDevice::Init() {
	Common::QueueFamilyIndices indices = Common::FindQueueFamilies(PhysicalDevice::Get());
	float queuePriority = 1.0f;
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = {
		indices.graphicsFamily.value(), indices.presentFamily.value()
	};

	for (uint32_t queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};

		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = indices.graphicsFamily.value();
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE;
	deviceFeatures.sampleRateShading = VK_TRUE; // Enable sample rate shading (msaa inside texture)

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(Common::_DeviceExtensions.size());
	createInfo.ppEnabledExtensionNames = Common::_DeviceExtensions.data();


	if (_enableValidationLayers) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(Common::_ValidationLayers.size());
		createInfo.ppEnabledLayerNames = Common::_ValidationLayers.data();
	} else {
		createInfo.enabledLayerCount = 0;
	}

	if (vkCreateDevice(PhysicalDevice::Get(), &createInfo, nullptr, &LogicalDevice::_vkDevice) != VK_SUCCESS) {
		throw std::runtime_error("Can't create a logical device");
	}

	_queues[LogicalDevice::QueueType::Graphics] = VK_NULL_HANDLE;
	_queues[LogicalDevice::QueueType::Present] = VK_NULL_HANDLE;

	//vkGetDeviceQueue(LogicalDevice::_vkDevice, indices.graphicsFamily.value(), 0, &Common::GraphicsQueue);
	vkGetDeviceQueue(LogicalDevice::_vkDevice, indices.graphicsFamily.value(), 0, &_queues[LogicalDevice::QueueType::Graphics]);
	vkGetDeviceQueue(LogicalDevice::_vkDevice, indices.presentFamily.value(), 0, &_queues[LogicalDevice::QueueType::Present]);
}

void LogicalDevice::Deinit() {

}

VkDevice LogicalDevice::_vkDevice;
std::map<LogicalDevice::QueueType, VkQueue> LogicalDevice::_queues;
