#include "PhysicalDevice.hpp"

#include "Instance.hpp"
#include "Common.hpp"

#include <set>
#include <stdexcept>
#include <algorithm>

const VkPhysicalDevice& PhysicalDevice::Get() {
	return PhysicalDevice::_vkPhysicalDevice;
}


void PhysicalDevice::Init() {
	PhysicalDevice::_vkPhysicalDevice = VK_NULL_HANDLE;

	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(Instance::Get(), &deviceCount, nullptr);

	if (!deviceCount) {
		throw std::runtime_error("Vulkan is not supported.");
	}

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(Instance::Get(), &deviceCount, devices.data());


	for (const auto& device : devices) {
		if (PhysicalDevice::_IsDeviceSuitable(device)) {
			PhysicalDevice::_vkPhysicalDevice = device;
			Common::MsaaSamples = PhysicalDevice::_GetMaxUsageSampleCount();
			break;
		}
	}

	if (PhysicalDevice::_vkPhysicalDevice == VK_NULL_HANDLE) {
		throw std::runtime_error("Vulkan is not supported.");
	}

	//PhysicalDevice::_instance = std::make_unique<PhysicalDevice>();
}

void PhysicalDevice::Deinit() {

}

// TODO: can returns a score to select the best one (page 63)
bool PhysicalDevice::_IsDeviceSuitable(const VkPhysicalDevice& device) {
	//VkPhysicalDeviceProperties deviceProperties;
	//vkGetPhysicalDeviceProperties(device, &deviceProperties);

	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

	Common::QueueFamilyIndices indices = Common::FindQueueFamilies(device);

	bool extensionsSupported = PhysicalDevice::_CheckDeviceExtensionsSupport(device);
	bool swapChainAdequate = false;

	if (extensionsSupported) {
		Common::SwapChainSupportDetails swapChainSupport = Common::QuerySwapChainSupport(device);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}
	return indices.isComplete() && extensionsSupported && swapChainAdequate && deviceFeatures.samplerAnisotropy;
}

bool PhysicalDevice::_CheckDeviceExtensionsSupport(VkPhysicalDevice device) {
	uint32_t extensionsCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionsCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionsCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionsCount, availableExtensions.data());

	std::set<std::string> requiredExtensions(Common::_DeviceExtensions.begin(), Common::_DeviceExtensions.end());

	for (const auto& extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();
}

VkSampleCountFlagBits PhysicalDevice::_GetMaxUsageSampleCount() {
	VkPhysicalDeviceProperties physicalProperties;
	vkGetPhysicalDeviceProperties(PhysicalDevice::Get(), &physicalProperties);

	VkSampleCountFlags counts = std::min(physicalProperties.limits.framebufferColorSampleCounts, physicalProperties.limits.framebufferDepthSampleCounts);
	if (counts & VK_SAMPLE_COUNT_64_BIT) {
		return VK_SAMPLE_COUNT_64_BIT;
	} else if (counts & VK_SAMPLE_COUNT_32_BIT) {
		return VK_SAMPLE_COUNT_32_BIT;
	} else if (counts & VK_SAMPLE_COUNT_16_BIT) {
		return VK_SAMPLE_COUNT_16_BIT;
	} else if (counts & VK_SAMPLE_COUNT_8_BIT) {
		return VK_SAMPLE_COUNT_8_BIT;
	} else if (counts & VK_SAMPLE_COUNT_4_BIT) {
		return VK_SAMPLE_COUNT_4_BIT;
	} else if (counts & VK_SAMPLE_COUNT_2_BIT) {
		return VK_SAMPLE_COUNT_2_BIT;
	}
	return VK_SAMPLE_COUNT_1_BIT;
}

VkPhysicalDevice PhysicalDevice::_vkPhysicalDevice;