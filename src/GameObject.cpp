#include "GameObject.hpp"



GameObject::GameObject() {
	GameObject::_NbGameObjects++;
	_name = "GameObject__" + std::to_string(GameObject::_NbGameObjects);
}

void GameObject::Update() {
	for (auto& updateScript : _updateScripts) {
		updateScript->Update(this);
	}
}

Transform& GameObject::transform() {
	return _transform;
}

void GameObject::AddUpdateScript(std::shared_ptr<UpdateScript> updateScript) {
	_updateScripts.push_back(updateScript);
}

const std::string& GameObject::name() const {
	return _name;
}

void GameObject::setName(const std::string& name) {
	_name = name;
}

int GameObject::_NbGameObjects = 0;
