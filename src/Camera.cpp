#include "Camera.hpp"
#include "Input.hpp"
#include "Time.hpp"

#include <algorithm>
#include <iostream>

#include <glm/gtx/string_cast.hpp>

Camera::Camera(int width, int height) {
	_transform.setPosition(glm::vec3(0.0f, 0.0f, 3.0f));
	_transform.setRotation(glm::vec3(0.0f, 0.0f, 0.0f));
	_front = glm::vec3(0.0f, 0.0f, -1.0f);
	_up = glm::vec3(0.0f, 1.0f, 0.0f);
	_type = Camera::Type::FREE;

	_width = width;
	_height = height;

	_far = 100.0f;
	_near = 0.01f;

	_speed = 0.5f;
	_sensitivity = 0.5f;
	_lastCursorPos = glm::vec2(width / 2.0f, height / 2.0f);
	_firstMouse = true;

	_updateProjectionMatrix();
	_updateViewMatrix();
}
const glm::mat4& Camera::view() const {
	return _view;
}

const glm::mat4& Camera::projection() const {
	return _proj;
}

float Camera::near() const {
	return _near;
}

float Camera::far() const {
	return _far;
}

void Camera::setNear(float near) {
	_near = near;
}

void Camera::setFar(float far) {
	_far = far;
}

void Camera::resize(int width, int height) {
	_width = width;
	_height = height;
	_lastCursorPos = glm::vec2(width / 2.0f, height / 2.0f);
	_updateProjectionMatrix();
}

void Camera::freeCameraRotation(float xPos, float yPos) {
	if (_type != Camera::Type::FREE) {
		return;
	}

	if (_firstMouse) {
		_lastCursorPos = { xPos, yPos };
		_firstMouse = false;
	}

	float xoffset = xPos - _lastCursorPos.x;
	float yoffset = _lastCursorPos.y - yPos;
	_lastCursorPos = { xPos, yPos };
	
	float sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	_transform.setRotation(glm::vec3(_transform.rotation().x + yoffset, _transform.rotation().y + xoffset, _transform.rotation().z));

	_transform.setRotation(glm::vec3(std::clamp(_transform.rotation().x, -89.0f, 89.0f), _transform.rotation().y, _transform.rotation().z));

	glm::vec3 front;
	front.x = -cos(glm::radians(_transform.rotation().x)) * sin(glm::radians(_transform.rotation().y));
	front.y = -sin(glm::radians(_transform.rotation().x));
	front.z = cos(glm::radians(_transform.rotation().x)) * cos(glm::radians(_transform.rotation().y));

	_front = -glm::normalize(front);
	_updateViewMatrix();
}

Camera::Type Camera::getType() const {
	return _type;
}

void Camera::setType(Camera::Type type) {
	_type = type;
}

void Camera::Update() {
	bool moved = false;

	if (Input::GetKey(Input::Key::W) == Input::State::PRESSED) {
		//_transform.translate({0.001f, 0.0f, 0.0f});
		_transform.setPosition(_transform.position() + _speed * _front * Time::DeltaTime);
		moved = true;
	}
	if (Input::GetKey(Input::Key::S) == Input::State::PRESSED) {
		_transform.setPosition(_transform.position() - _speed * _front * Time::DeltaTime);
		moved = true;
	}
	if (Input::GetKey(Input::Key::A) == Input::State::PRESSED) {
		_transform.setPosition(_transform.position() - glm::normalize(glm::cross(_front, _up)) * _speed * Time::DeltaTime);
		moved = true;
	}
	if (Input::GetKey(Input::Key::D) == Input::State::PRESSED) {
		_transform.setPosition(_transform.position() + glm::normalize(glm::cross(_front, _up)) * _speed * Time::DeltaTime);
		moved = true;
	}
	if (Input::GetKey(Input::Key::Q) == Input::State::PRESSED) {
		_transform.setPosition(_transform.position() - _speed * _up * Time::DeltaTime);
		moved = true;
	}
	if (Input::GetKey(Input::Key::E) == Input::State::PRESSED) {
		_transform.setPosition(_transform.position() + _speed * _up * Time::DeltaTime);
		moved = true;
	}

	if (moved) {
		_updateViewMatrix();
	}

}

void Camera::_updateProjectionMatrix() {
	_proj = glm::perspective(glm::radians(45.0f), _width / static_cast<float>(_height), _near, _far);
	_proj[1][1] *= -1; // because vulkan and not opengl
}

void Camera::_updateViewMatrix() {
	_view = glm::lookAt(_transform.position(), _transform.position() + _front, _up);
}