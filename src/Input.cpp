#include "Input.hpp"

Input::State Input::GetKey(Input::Key key) {
	int res = glfwGetKey(Input::window, _keys[key]);
	if (res == GLFW_PRESS) {
		return State::PRESSED;
	} else if (res == GLFW_RELEASE) {
		return State::RELEASED;
	}
	return State::UNDEFINED;
}

GLFWwindow* Input::window = nullptr;

std::map<Input::Key, int> Input::_keys = {
	{ Input::Key::A, GLFW_KEY_A },
	{ Input::Key::B, GLFW_KEY_B },
	{ Input::Key::C, GLFW_KEY_C },
	{ Input::Key::D, GLFW_KEY_D },
	{ Input::Key::D, GLFW_KEY_D },
	{ Input::Key::E, GLFW_KEY_E },
	{ Input::Key::F, GLFW_KEY_F },
	{ Input::Key::G, GLFW_KEY_G },
	{ Input::Key::H, GLFW_KEY_H },
	{ Input::Key::I, GLFW_KEY_I },
	{ Input::Key::J, GLFW_KEY_J },
	{ Input::Key::K, GLFW_KEY_K },
	{ Input::Key::L, GLFW_KEY_L },
	{ Input::Key::M, GLFW_KEY_M },
	{ Input::Key::N, GLFW_KEY_N },
	{ Input::Key::O, GLFW_KEY_O },
	{ Input::Key::P, GLFW_KEY_P },
	{ Input::Key::Q, GLFW_KEY_Q },
	{ Input::Key::R, GLFW_KEY_R },
	{ Input::Key::S, GLFW_KEY_S },
	{ Input::Key::T, GLFW_KEY_T },
	{ Input::Key::U, GLFW_KEY_U },
	{ Input::Key::V, GLFW_KEY_V },
	{ Input::Key::W, GLFW_KEY_W },
	{ Input::Key::X, GLFW_KEY_X },
	{ Input::Key::Y, GLFW_KEY_Y },
	{ Input::Key::Z, GLFW_KEY_Z },
	{ Input::Key::K0, GLFW_KEY_0 },
	{ Input::Key::K1, GLFW_KEY_1 },
	{ Input::Key::K2, GLFW_KEY_2 },
	{ Input::Key::K3, GLFW_KEY_3 },
	{ Input::Key::K4, GLFW_KEY_4 },
	{ Input::Key::K5, GLFW_KEY_5 },
	{ Input::Key::K6, GLFW_KEY_6 },
	{ Input::Key::K7, GLFW_KEY_7 },
	{ Input::Key::K8, GLFW_KEY_8 },
	{ Input::Key::K9, GLFW_KEY_9 },
	{ Input::Key::LeftCtrl, GLFW_KEY_LEFT_CONTROL },
	{ Input::Key::RightCtrl, GLFW_KEY_RIGHT_CONTROL },
	{ Input::Key::RightMaj, GLFW_KEY_RIGHT_SHIFT },
	{ Input::Key::LeftMaj, GLFW_KEY_LEFT_SHIFT },
	{ Input::Key::LeftAlt, GLFW_KEY_LEFT_ALT },
	{ Input::Key::RightAlt, GLFW_KEY_RIGHT_ALT }
};
